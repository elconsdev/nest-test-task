import { ApiProperty } from '@nestjs/swagger';

export class CreateTickerDto {
  @ApiProperty({ description: 'Name' })
  readonly name: string;
}
