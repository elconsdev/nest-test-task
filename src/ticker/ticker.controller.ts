import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { TickerService } from './ticker.service';
import { TickerEntity } from './ticker.entity';
import { UserEntity } from '../users/user.entity';
import { CreateTickerDto } from './dto/createTicker.dto';

@ApiTags('Ticker')
@Controller('api/ticker')
export class TickerController {
  constructor(private tickerService: TickerService) {}

  @ApiOperation({ summary: 'Get all tickers' })
  @ApiResponse({ status: 200, type: [TickerEntity] })
  @Get()
  getAll() {
    return this.tickerService.getAll();
  }

  @ApiOperation({ summary: 'Create new ticker' })
  @ApiResponse({ status: 200, type: UserEntity })
  @Post()
  create(@Body() createTickerDto: CreateTickerDto) {
    return this.tickerService.create(createTickerDto);
  }
}
