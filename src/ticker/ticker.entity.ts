import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'ticker' })
export class TickerEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar' })
  name: string;
}
