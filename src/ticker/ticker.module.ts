import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TickerEntity } from './ticker.entity';
import { TickerController } from './ticker.controller';
import { TickerService } from './ticker.service';

@Module({
  imports: [TypeOrmModule.forFeature([TickerEntity])],
  controllers: [TickerController],
  providers: [TickerService],
})
export class TickerModule {}
