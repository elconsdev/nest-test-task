import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TickerEntity } from './ticker.entity';
import { Repository } from 'typeorm';
import { CreateTickerDto } from './dto/createTicker.dto';

@Injectable()
export class TickerService {
  constructor(
    @InjectRepository(TickerEntity)
    private tickerRepository: Repository<TickerEntity>,
  ) {}

  getAll(): Promise<TickerEntity[]> {
    return this.tickerRepository.find();
  }

  findOne(id: string): Promise<TickerEntity> {
    return this.tickerRepository.findOne(id);
  }

  create(dto: CreateTickerDto): Promise<TickerEntity> {
    const user = new TickerEntity();
    user.name = dto.name;
    return this.tickerRepository.save(user);
  }
}
